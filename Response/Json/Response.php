<?php
/**
 * Created by PhpStorm.
 * User: btica
 * Date: 15/11/18
 * Time: 23:07
 */

namespace Response\Json;

use Header\Header;
use Response\AbstractResponse;

/**
 * Class Response
 * @package Response\Json
 */
class Response extends AbstractResponse
{

    /**
     * Response constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->headers[] = new Header('Content-Type', 'application/json');
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        parent::setContent(is_string($content) ? $content : json_encode($content));
    }
}