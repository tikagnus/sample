<?php
/**
 * Created by PhpStorm.
 * User: btica
 * Date: 15/11/18
 * Time: 23:33
 */

namespace Response\Contracts;

/**
 * Interface ResponseInterface
 * @package Response\Contracts
 */
interface ResponseInterface
{
    /**
     *
     */
    const HTTP_OK = 200;
    /**
     *
     */
    const HTTP_ACCEPTED = 202;
}