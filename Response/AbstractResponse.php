<?php
/**
 * Created by PhpStorm.
 * User: btica
 * Date: 15/11/18
 * Time: 23:02
 */

namespace Response;

use Header\AbstractHeader;
use Response\Contracts\ResponseInterface;

abstract class AbstractResponse implements ResponseInterface
{
    /**
     * @var AbstractHeader[]
     */
    protected $headers;

    /**
     * @var int
     */
    protected $status = ResponseInterface::HTTP_OK;

    /**
     * @var mixed
     */
    protected $content;

    /**
     * Response constructor.
     */
    public function __construct()
    {
        $this->headers = [];
    }

    /**
     * @return AbstractHeader[]
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param AbstractHeader[] $headers
     */
    public function setHeaders(array $headers): void
    {
        $this->headers = $headers;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }
}
