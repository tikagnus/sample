<?php
/**
 * Created by PhpStorm.
 * User: btica
 * Date: 15/11/18
 * Time: 23:07
 */

namespace Response\Html;

use Header\Header;
use Response\AbstractResponse;

/**
 * Class Response
 * @package Response\Html
 */
class Response extends AbstractResponse
{
    /**
     * Response constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->headers[] = new Header('Content-Type', 'text/html');
    }
}