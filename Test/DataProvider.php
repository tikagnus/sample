<?php

namespace Test;

use Header\AbstractHeader;
use Request\Contracts\Methods\Methods;

/**
 * Created by PhpStorm.
 * User: btica
 * Date: 15/11/18
 * Time: 22:27
 */
abstract class DataProvider implements Methods
{
    /**
     * @return string
     */
    abstract public function getMethod(): string;

    /**
     * @return string
     */
    abstract public function getUri(): string;

    /**
     * @return AbstractHeader[]
     */
    abstract public function getHeaders(): array;
}