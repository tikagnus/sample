<?php
/**
 * Created by PhpStorm.
 * User: btica
 * Date: 15/11/18
 * Time: 21:57
 */

namespace Request\Contracts\Methods;

use Header\AbstractHeader;

interface Methods
{
    /**
     *
     */
    const GET = "GET";

    /**
     *
     */
    const POST = "GET";
    /*....*/

    /**
     * @return string
     */
    public function getMethod(): string;

    /**
     * @return string
     */
    public function getUri(): string;

    /**
     * @return AbstractHeader[]
     */
    public function getHeaders(): array;
}