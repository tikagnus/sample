<?php
/**
 * Created by PhpStorm.
 * User: btica
 * Date: 15/11/18
 * Time: 21:49
 */

namespace Request;

use Header\AbstractHeader;
use Request\Contracts\Methods\Methods;

/**
 * Class AbstractRequest
 * @package Request
 */
abstract class AbstractRequest implements Methods
{
    /**
     * @var string
     */
    protected $method;
    /**
     * @var string
     */
    protected $uri;

    /**
     * @var AbstractHeader[]
     */
    protected $headers;

    /**
     * AbstractRequest constructor.
     */
    public function __construct()
    {
        $this->headers = [];
        $this->initMethod();
        $this->initUri();
        $this->initHeaders();
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return void
     */
    abstract protected function initMethod();

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @return void
     */
    abstract protected function initUri();

    /**
     * @return AbstractHeader[]
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @return void
     */
    abstract protected function initHeaders();
}