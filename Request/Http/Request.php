<?php
/**
 * Created by PhpStorm.
 * User: btica
 * Date: 15/11/18
 * Time: 22:15
 */

namespace Request\Http;

use Header\Header;
use Request\AbstractRequest;

class Request extends AbstractRequest
{

    /**
     * @return void
     */
    protected function initMethod()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
    }

    /**
     * @return void
     */
    protected function initUri()
    {
        $this->uri = $_SERVER['REQUEST_URI'];
    }

    /**
     * @return void
     */
    public function initHeaders()
    {
        foreach (getallheaders() as $header => $content) {
            $this->headers[] = new Header($header, $content);
        }
    }
}