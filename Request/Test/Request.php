<?php
/**
 * Created by PhpStorm.
 * User: btica
 * Date: 15/11/18
 * Time: 22:26
 */

namespace Request\Test;

use Request\AbstractRequest;
use Test\DataProvider;

class Request extends AbstractRequest
{
    /**
     * @var DataProvider
     */
    protected $dataProvider;

    /**
     * @return void
     */
    protected function initMethod()
    {
        $this->method = $this->dataProvider->getMethod();
    }

    /**
     * @return void
     */
    protected function initUri()
    {
        $this->uri = $this->dataProvider->getUri();
    }

    /**
     * @return void
     */
    protected function initHeaders()
    {
        $this->headers = $this->dataProvider->getHeaders();
    }
}