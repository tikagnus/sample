<?php
/**
 * Created by PhpStorm.
 * User: btica
 * Date: 15/11/18
 * Time: 21:45
 */

namespace Controller;

use Request\AbstractRequest;

/**
 * Class Controller
 * @package Controller
 */
abstract class AbstractController
{
    /**
     * @var AbstractRequest
     */
    protected $request;

    /**
     * Controller constructor.
     * @param AbstractRequest $request
     */
    public function __construct(AbstractRequest $request)
    {
        $this->request = $request;
    }
}
